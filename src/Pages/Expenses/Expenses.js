import React from "react";
import "./Expenses.css";
import AddExpenses from "./AddExpenses";
import DeleteExpense from "./DeleteExpense";
import EditExpense from "./EditExpense";
import Loader from "../../components/Loader";
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import axios from "axios";

const Expenses = () => {
  const [expense, setExpense] = useState([]);
  const [category, setCategoryy] = useState([]);
  const [reload, setReload] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [pageNumber, setPageNumber] = useState(0);

  const token = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  const getAllExpense = async () => {
    try {
      const response = await axios.get(
        "https://boiling-escarpment-75862.herokuapp.com/api/expenses",
        {
          config,
        }
      );
      console.log(response.data);
      setIsLoading(false);
      setExpense(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getFixedExpense = async () => {
    try {
      const response = await axios.get(
        "https://boiling-escarpment-75862.herokuapp.com/api/expenses/search/fixed",
        {
          config,
        }
      );
      console.log(response.data);
      setExpense(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getRecurringExpense = async () => {
    try {
      const response = await axios.get(
        "https://boiling-escarpment-75862.herokuapp.com/api/expenses/search/recurring",
        {
          config,
        }
      );
      console.log(response.data);
      setExpense(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  async function getCategory() {
    const response = await axios.get(
      "https://boiling-escarpment-75862.herokuapp.com/api/categories",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log(response.data.categories);
    setCategoryy(response.data.categories);
  }

  useEffect(() => {
    getAllExpense();
    getCategory();
  }, [reload]);

  if (isLoading) {
    return <Loader />;
  }

  const expensePerPage = 10;
  const pagesVisited = pageNumber * expensePerPage;

  const displayExpenses = expense.slice(
    pagesVisited,
    pagesVisited + expensePerPage
  );

  const pageCount = Math.ceil(expense.length / expensePerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <>
      <div className="header__table">
        <AddExpenses
          data={expense}
          category={category}
          reload={reload}
          setReload={setReload}
        />
        <div className="buttons__filtering">
          <button className="button-searching" onClick={getAllExpense}>
            All
          </button>
          <button className="button-searching" onClick={getFixedExpense}>
            Fixed
          </button>
          <button className="button-searching" onClick={getRecurringExpense}>
            Recurring
          </button>
        </div>
      </div>
      <table className="table table-action">
        <thead>
          <tr>
            <th className="t-small"></th>
            <th className="t-small"></th>
            <th className="t-title">Title</th>
            <th className="t-medium">Description</th>
            <th className="t-small">Amount</th>
            <th className="t-date">Date</th>
            <th className="t-small">Type</th>
            <th className="t-small">Category</th>
            <th className="t-small">Currency</th>
            <th className="t-date">Start Date</th>
            <th className="t-date">End Date</th>
          </tr>
        </thead>
        {isLoading ? (
          <Loader />
        ) : (
          <tbody>
            {displayExpenses.map((data) => {
              return (
                <tr key={data.id}>
                  <td>
                    <DeleteExpense
                      data={data}
                      reload={reload}
                      setReload={setReload}
                    />
                  </td>
                  <td>
                    <EditExpense
                      data={data}
                      category={category}
                      reload={reload}
                      setReload={setReload}
                    />
                  </td>
                  <td>{data.title}</td>
                  <td>{data.description}</td>
                  <td>{data.amount}</td>
                  <td>{data.date}</td>
                  <td>{data.type}</td>
                  <td>{data.categories.name}</td>
                  <td>{data.currency}</td>
                  <td>{data.start_date}</td>
                  <td>{data.end_date}</td>
                </tr>
              );
            })}
          </tbody>
        )}
      </table>
      <ReactPaginate
        previousLabel={"<<"}
        nextLabel={">>"}
        breakLabel={"..."}
        pageCount={pageCount}
        marginPagesDisplayed={1}
        pageRangeDisplayed={1}
        onPageChange={changePage}
        containerClassName={"paginationBttns"}
        previousLinkClassName={"previousBttn"}
        nextLinkClassName={"nextBttn"}
        disabledClassName={"paginationDisabled"}
        activeClassName={"paginationActive"}
      />
    </>
  );
};

export default Expenses;
