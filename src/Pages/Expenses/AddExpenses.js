import React, { useState } from "react";
import axios from "axios";
import { IoMdAddCircle } from "react-icons/io";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

const AddExpenses = (props) => {
  const [open, setOpen] = React.useState(false);
  const [amount, setAmount] = useState("");
  const [currency, setCurrency] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [category_id, setCategory] = useState("");
  const [type, setType] = useState("");
  const [date, setDate] = useState("");
  const [start_date, setStartDate] = useState("");
  const [end_date, setEndDate] = useState("");
  const [showRecurring, setShowRecurring] = useState(false);

  const token = localStorage.getItem("token");

  const PostRequestHandler = async () => {
    const data = {
      title,
      description,
      amount,
      date,
      type,
      category_id,
      currency,
      start_date,
      end_date,
    };
    console.log("Data", data);

    await axios
      .post(
        "https://boiling-escarpment-75862.herokuapp.com/api/expenses",
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .catch((err) => {
        console.log(err);
      });
    setOpen(false);
    handleClose();
    props.setReload(props.data);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IoMdAddCircle
        className="Add__button"
        size={33}
        onClick={handleClickOpen}
      />
      <Dialog
        maxWidth={"md"}
        sx={{ overflowY: "scroll" }}
        open={open}
        onClose={handleClose}
      >
        <DialogTitle>Add Expenses</DialogTitle>
        <DialogContent>
          <div className="container-income">
            <br />
            <label>Title</label>
            <br />
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <br />
            <label>Amount</label>
            <br />
            <input
              type="number"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />
            <br />
            <label>Currency</label>
            <br />
            <select
              id="type"
              type="radio"
              value={currency}
              onChange={(e) => setCurrency(e.target.value)}
            >
              <option value="USD">USD</option>
              <option value="LBP">LBP</option>
            </select>
            <br />
            <label>Description</label>
            <br />
            <input
              type="description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
            <br />
            <label>Type</label>
            <br />
            <br />
            <div value={type} onChange={(e) => setType(e.target.value)}>
               {" "}
              <input
                type="radio"
                id="html"
                name="type"
                value="fixed"
                onClick={() => setShowRecurring(false)}
              />
              <label htmlFor="html">Fixed</label> {" "}
              <input
                type="radio"
                id="css"
                name="type"
                value="recurring"
                onClick={() => setShowRecurring(true)}
              />
                <label htmlFor="css">Recurring</label>
              <br />
              <br />
              {showRecurring === true && (
                <div>
                  <label>Start Date</label>
                  <br />
                  <input
                    type="date"
                    name="date"
                    value={start_date}
                    onChange={(e) => setStartDate(e.target.value)}
                  />
                  <br />
                  <label>End Date</label>
                  <br />
                  <input
                    type="date"
                    name="date"
                    value={end_date}
                    onChange={(e) => setEndDate(e.target.value)}
                  />
                </div>
              )}
            </div>
            <br />
            <label>Category</label>
            <br />
            <select
              value={category_id}
              onChange={(e) => setCategory(e.target.value)}
            >
              {props.category &&
                props.category.map((item, index) => {
                  return (
                    <option value={item.id} key={index}>
                      {item.name}
                    </option>
                  );
                })}
            </select>
            <br />
            <label>Date</label>
            <br />
            <input
              type="date"
              name="date"
              value={date}
              onChange={(e) => setDate(e.target.value)}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <button className="dialog__buttons" onClick={handleClose}>
            Cancel
          </button>
          <button className="dialog__buttons" onClick={PostRequestHandler}>
            Add
          </button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddExpenses;
