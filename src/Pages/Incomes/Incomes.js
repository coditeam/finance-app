import React from "react";
import "./Incomes.css";
import { useEffect, useState } from "react";
import axios from "axios";

import ReactPaginate from "react-paginate";
import Loader from "../../components/Loader";
import AddIncomes from "./AddIncomes";
import DeleteIncomes from "./DeleteIncomes";
import EditIncomes from "./EditIncomes";
// require("dotenv").config();

const Incomes = () => {
  const [income, setIncome] = useState([]);
  const [category, setCategoryy] = useState([]);
  const [reload, setReload] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [pageNumber, setPageNumber] = useState(0);

  const token = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };

  const getAllIncome = async () => {
    try {
      const response = await axios.get(
        "https://boiling-escarpment-75862.herokuapp.com/api/incomes",
        {
          config,
        }
      );
      console.log(response.data);
      setIsLoading(false);
      setIncome(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getFixedIncome = async () => {
    try {
      const response = await axios.get(
        "https://boiling-escarpment-75862.herokuapp.com/api/incomes/search/fixed",
        {
          config,
        }
      );
      console.log(response.data);
      setIsLoading(false);
      setIncome(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getRecurringIncome = async () => {
    try {
      const response = await axios.get(
        "https://boiling-escarpment-75862.herokuapp.com/api/incomes/search/recurring",
        {
          config,
        }
      );
      console.log(response.data);
      setIncome(response.data.data);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  async function getCategory() {
    const response = await axios.get(
      "https://boiling-escarpment-75862.herokuapp.com/api/categories",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log(response.data.categories);
    setCategoryy(response.data.categories);
  }

  useEffect(() => {
    getAllIncome();
    getCategory();
  }, [reload]);

  if (isLoading) {
    return <Loader />;
  }

  const incomesPerPage = 10;
  const pagesVisited = pageNumber * incomesPerPage;

  const displayUsers = income.slice(
    pagesVisited,
    pagesVisited + incomesPerPage
  );

  const pageCount = Math.ceil(income.length / incomesPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <>
      <div className="Incomes__page">
        <div className="header__table">
          <AddIncomes data={income} category={category} setReload={setReload} />
          <div className="buttons__filtering" activeclassName="active__button">
            <button className="button-searching" onClick={getAllIncome}>
              All
            </button>
            <button className="button-searching" onClick={getFixedIncome}>
              Fixed
            </button>
            <button className="button-searching" onClick={getRecurringIncome}>
              Recurring
            </button>
          </div>
        </div>
        <table className="table table-action">
          <thead>
            <tr>
              <th className="t-small"></th>
              <th className="t-small"></th>
              <th className="t-title">Title</th>
              <th className="t-medium">Description</th>
              <th className="t-small">Amount</th>
              <th className="t-date">Date</th>
              <th className="t-small">Type</th>
              <th className="t-small">Category</th>
              <th className="t-small">Currency</th>
              <th className="t-date">Start Date</th>
              <th className="t-date">End Date</th>
            </tr>
          </thead>
          {isLoading ? (
            <Loader />
          ) : (
            <tbody>
              {displayUsers.map((data) => {
                return (
                  <tr key={data.id}>
                    <td>
                      <DeleteIncomes data={data} setReload={setReload} />
                    </td>
                    <td>
                      <EditIncomes
                        data={data}
                        category={category}
                        setReload={setReload}
                      />
                    </td>
                    <td>{data.title}</td>
                    <td>{data.description}</td>
                    <td>{data.amount}</td>
                    <td>{data.date}</td>
                    <td>{data.type}</td>
                    <td>{data.categories.name}</td>
                    <td>{data.currency}</td>
                    <td>{data.start_date}</td>
                    <td>{data.end_date}</td>
                  </tr>
                );
              })}
            </tbody>
          )}
        </table>
        <ReactPaginate
          previousLabel={"<<"}
          nextLabel={">>"}
          breakLabel={"..."}
          pageCount={pageCount}
          marginPagesDisplayed={1}
          pageRangeDisplayed={1}
          onPageChange={changePage}
          containerClassName={"paginationBttns"}
          previousLinkClassName={"previousBttn"}
          nextLinkClassName={"nextBttn"}
          disabledClassName={"paginationDisabled"}
          activeClassName={"paginationActive"}
        />
      </div>
    </>
  );
};

export default Incomes;
