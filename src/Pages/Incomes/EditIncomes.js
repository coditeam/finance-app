import React from "react";
import "./Incomes.css";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import { AiFillEdit } from "react-icons/ai";
import { useState, useEffect } from "react";
import axios from "axios";

const EditIncomes = (props) => {
  const [open, setOpen] = React.useState(false);
  const [amount, setAmount] = useState(props.data.amount);
  const [currency, setCurrency] = useState(props.data.currency);
  const [title, setTitle] = useState(props.data.title);
  const [description, setDescription] = useState(props.data.description);
  const [category_id, setCategory] = useState(props.data.category_id);
  const [type, setType] = useState(props.data.type);
  const [date, setDate] = useState(props.data.date);
  const [start_date, setStartDate] = useState(props.data.start_date);
  const [end_date, setEndDate] = useState(props.data.end_date);
  const [showRecurring, setShowRecurring] = useState(false);

  const token = localStorage.getItem("token");

  const editIncome = () => {
    axios
      .put(
        `https://boiling-escarpment-75862.herokuapp.com/api/incomes/${props.data.id}`,
        {
          amount,
          currency,
          title,
          description,
          category_id,
          type,
          date,
          start_date,
          end_date,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(() => setOpen(false))
      .catch((err) => console.log(err));
    props.setReload(props.data);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    editIncome();
  }, []);

  return (
    <div>
      <AiFillEdit
        onClick={handleClickOpen}
        size={30}
        className="iconss__func"
      />
      <Dialog maxWidth={"md"} open={open} onClose={handleClose}>
        <DialogTitle>Edit Incomes</DialogTitle>
        <DialogContent>
          <div className="container-income">
            <br />
            <label>Title</label>
            <br />
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
            <br />
            <label>Amount</label>
            <br />
            <input
              type="number"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />
            <br />
            <label>Currency</label>
            <br />
            <select
              id="type"
              type="option"
              value={currency}
              onChange={(e) => setCurrency(e.target.value)}
            >
              <option value="$">$</option>
              <option value="LL">LL</option>
            </select>

            <br />
            <label>Description</label>
            <br />
            <input
              type="description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
            <br />
            <label>Type</label>
            <br />
            <div value={type} onChange={(e) => setType(e.target.value)}>
              <br />
              <input
                type="radio"
                id="html"
                name="type"
                value="fixed"
                onClick={() => setShowRecurring(false)}
              />
              <label htmlFor="html">Fixed</label>
              <input
                type="radio"
                id="css"
                name="type"
                value="recurring"
                onClick={() => setShowRecurring(true)}
              />
              <label htmlFor="css">Recurring</label>
            </div>
            <br />
            {showRecurring === true && (
              <div>
                <label>Start Date</label>
                <br />
                <input
                  type="date"
                  name="date"
                  value={start_date}
                  onChange={(e) => setStartDate(e.target.value)}
                />
                <br />
                <label>End Date</label>
                <br />
                <input
                  type="date"
                  name="date"
                  value={end_date}
                  onChange={(e) => setEndDate(e.target.value)}
                />
              </div>
            )}
            <br />
            <label>Category</label>
            <br />
            <select
              value={category_id}
              onChange={(e) => setCategory(e.target.value)}
            >
              {props.category &&
                props.category.map((item) => {
                  return (
                    <option value={item.id} key={item.id}>
                      {item.name}
                    </option>
                  );
                })}
            </select>
            <br />
            <label>Date</label>
            <br />
            <input
              type="date"
              name="date"
              value={date}
              onChange={(e) => setDate(e.target.value)}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <button onClick={handleClose} className="dialog__buttons">
            Cancel
          </button>
          <button onClick={editIncome} className="dialog__buttons">
            Edit
          </button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EditIncomes;
