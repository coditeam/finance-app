import * as React from "react";
import axios from "axios";
import { useEffect } from "react";

// Import Material UI
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import { AiTwotoneDelete } from "react-icons/ai";

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const token = localStorage.getItem("token");

  const DeleteIncomes = () => {
    axios.delete(
      `https://boiling-escarpment-75862.herokuapp.com/api/incomes/${props.data.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    props.setReload(props.data);
    handleClose();
  };

  // useEffect(() => {
  //   DeleteIncomes();
  // }, []);

  return (
    <div>
      {/* <button onClick={handleClickOpen}> */}
      <AiTwotoneDelete
        onClick={handleClickOpen}
        size={27}
        className="iconss__func"
      />
      {/* </button> */}
      <Dialog
        maxWidth={"md"}
        sx={{ overflowY: "scroll" }}
        open={open}
        onClose={handleClose}
      >
        <DialogActions>
          <p>Are you sure you want to delete it?</p>
          <br />
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={DeleteIncomes}>Delete</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
