import { useState, useEffect } from "react";
import { Grid, Paper, Avatar, TextField, Button } from "@material-ui/core";
import { AiFillLock, AiFillUnlock } from "react-icons/ai";
// import { LockOutlinedIcon } from "@material-ui/icons/LockOutlinedIcon";
import { useNavigate } from "react-router-dom";
import "./LoginPage.css";
// import { Container } from "@mui/system";

const LoginPage = () => {
  const [isSubmitted] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [lock, setLock] = useState(false);
  const [setIsLoggedin] = useState(false);

  const navigate = useNavigate();
  const token = localStorage.getItem("token");

  const parseJwt = (token) => {
    try {
      return JSON.parse(atob(token.split(".")[1]));
    } catch (e) {
      return null;
    }
  };

  useEffect(() => {
    if (localStorage.getItem("token") !== null) {
      navigate("/", { replace: true });
    }
  }, []);

  const handleSubmit = async () => {
    try {
      const response = await fetch(
        "https://boiling-escarpment-75862.herokuapp.com/api/login",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            email: email,
            password: password,
          }),
        }
      );
      const data = await response.json();
      console.log(data);
      // if (data.success === false) {
      //   setError(data.message);
      // } else {
      // setIsLoggedin(true);
      const token = data.token;
      localStorage.setItem("token", token);
      const id = parseJwt(token).sub;
      localStorage.setItem("id", id);
      navigate("/dashboard/superadmin", { replace: true });
      // }
    } catch (error) {
      console.log(error);
    }
  };

  const paperStyle = {
    padding: 20,
    height: "70vh",
    width: "280px",
    margin: "25px auto",
  };

  const avatarStyle = {
    backgroundColor: "#2b2b2b",
  };

  const toggle = () => {
    setLock(!lock);
  };

  // JSX code for login form

  const renderForm = (
    <div className="login__form">
      <Grid container spacing={24} align="center" className="grid__login__form">
        <Paper elevation={10} style={paperStyle}>
          <Grid align="center">
            <Avatar style={avatarStyle}>
              {lock ? <AiFillUnlock /> : <AiFillLock />}
            </Avatar>
            <h2>Login</h2>
            <p>
              {error && (
                <div className="error" style={{ color: "red" }}>
                  {error}
                </div>
              )}
            </p>
          </Grid>
          <TextField
            label="Username"
            placeholder="Enter username"
            fullWidth
            required
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            label="Password"
            placeholder="Enter password"
            type="password"
            fullWidth
            required
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button
            // onClick={handleSubmit}

            onClick={() => {
              handleSubmit();
              toggle();
            }}
            variant="contained"
            disableElevation
            container
            justify="center"
          >
            Login
          </Button>
        </Paper>
      </Grid>
    </div>
  );

  return (
    <div className="loginPageContainer">
      {isSubmitted ? <div>User is successfully logged in</div> : renderForm}
    </div>
  );
};

export default LoginPage;
