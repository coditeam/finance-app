import React, { useEffect, useState } from "react";
import axios from "axios";
import Loader from "../../components/Loader";
import AddCategory from "./AddCategory";
import DeleteCategory from "./DeleteCategory";
import EditCategory from "./EditCategory";
import "./Category.css";

const Category = () => {
  const [category, setCategory] = useState([]);
  const [reload, setReload] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const token = localStorage.getItem("token");

  async function getCategory() {
    const response = await axios.get(
      "https://boiling-escarpment-75862.herokuapp.com/api/categories",
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log(response.data.categories);
    setIsLoading(false);
    setCategory(response.data.categories);
  }

  useEffect(() => {
    getCategory();
  }, [reload]);

  if (isLoading) {
    return <Loader />;
  }

  return (
    <>
      <div className="header__table">
        <AddCategory reload={reload} setReload={setReload} />
        {/*Add button Component*/}
      </div>
      <table className="table table-action">
        <thead>
          <tr>
            <th className="t-icon"></th>
            <th className="t-icon"></th>
            <th className="t-small">Name</th>
            <th className="t-small">Type</th>
            <th className="t-medium">Description</th>
          </tr>
        </thead>
        {isLoading ? (
          <Loader />
        ) : (
          <tbody>
            {category.map((data) => {
              return (
                <tr key={data.id}>
                  <td>
                    <DeleteCategory
                      data={data}
                      reload={reload}
                      setReload={setReload}
                    />
                    {/*Delete button Component*/}
                  </td>
                  <td>
                    <EditCategory
                      data={data}
                      reload={reload}
                      setReload={setReload}
                    />
                    {/*Edit button Component*/}
                  </td>
                  <td>{data.name}</td>
                  <td>{data.type}</td>
                  <td>{data.description}</td>
                </tr>
              );
            })}
          </tbody>
        )}
      </table>
    </>
  );
};

export default Category;
