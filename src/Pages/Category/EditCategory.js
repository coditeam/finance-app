import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import { AiFillEdit } from "react-icons/ai";
import { useState, useEffect } from "react";
import axios from "axios";

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [name, setName] = useState(props.data.name);
  const [type, setType] = useState(props.data.type);
  const [description, setDescription] = useState(props.data.description);

  const token = localStorage.getItem("token");

  const editCategory = () => {
    axios
      .put(
        `https://boiling-escarpment-75862.herokuapp.com/api/categories/${props.data.id}`,
        {
          name,
          type,
          description,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )

      .then(() => setOpen(false))
      .catch((err) => console.log(err));
    props.setReload(props.data);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    editCategory();
  }, []);

  return (
    <div>
      <AiFillEdit
        size={30}
        className="iconss__func"
        onClick={handleClickOpen}
      />
      <Dialog
        fullWidth={true}
        sx={{ overflowY: "scroll" }}
        open={open}
        onClose={handleClose}
      >
        <DialogContent>
          <label>Name</label>
          <br />
          <input
            type="description"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <br />
          <div>
            <br />
            <label>Type:</label>
            <br />
            <div value={type} onChange={(e) => setType(e.target.value)}>
                <input type="radio" id="html" name="type" value="income" />
              <label htmlFor="html">income</label>
                <input
                type="radio"
                id="css"
                name="type"
                value="expense"
              />  <label htmlFor="css">expense</label>
            </div>
          </div>
          <label>Description</label>
          <br />
          <input
            type="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <br />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={editCategory}>Edit</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
