import * as React from "react";
import { useState } from "react";
import axios from "axios";

// Material UI
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import { IoMdAddCircle } from "react-icons/io";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

export default function AddCategory(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [description, setDescription] = useState("");

  const token = localStorage.getItem("token");

  const postRequestHandler = async () => {
    const data = { name, type, description };

    console.log("Data: ", data);

    await axios

      .post(
        "https://boiling-escarpment-75862.herokuapp.com/api/categories",
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .catch((err) => console.log(err));
    handleClose();
    props.setReload(props.data);
  };

  return (
    <div>
      <IoMdAddCircle
        className="Add__button"
        size={33}
        onClick={handleClickOpen}
      />

      <Dialog
        fullWidth={true}
        sx={{ overflowY: "scroll" }}
        open={open}
        onClose={handleClose}
      >
        <DialogTitle>Add Category</DialogTitle>

        <DialogContent>
          <div className="container-category">
            <br />
            <label>Name</label>
            <br />
            <input type="text" onChange={(e) => setName(e.target.value)} />
            <br />
            <br />
            <label>Description</label>
            <br />
            <input
              type="text"
              onChange={(e) => setDescription(e.target.value)}
            />
            <br />
            <label>Type</label>
            <br />
            <div value={type} onChange={(e) => setType(e.target.value)}>
                <input type="radio" id="html" name="type" value="income" />
              <label htmlFor="html">income</label>
                <input
                type="radio"
                id="css"
                name="type"
                value="expense"
              />  <label htmlFor="css">expense</label>
            </div>
            <br />
          </div>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={postRequestHandler}>Add</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
