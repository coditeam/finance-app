import * as React from "react";
import { useEffect } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import axios from "axios";
import { AiTwotoneDelete } from "react-icons/ai";

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const token = localStorage.getItem("token");

  const deleteCategory = () => {
    axios.delete(
      `https://boiling-escarpment-75862.herokuapp.com/api/categories/${props.data.id}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    handleClose();
    props.setReload(props.data);
  };

  // useEffect(() => {
  //   deleteCategory();
  // }, []);

  return (
    <div>
      <AiTwotoneDelete
        onClick={handleClickOpen}
        size={27}
        className="iconss__func"
      />

      <Dialog
        maxWidth={"md"}
        sx={{ overflowY: "scroll" }}
        open={open}
        onClose={handleClose}
      >
        <DialogActions>
          <p>Are you sure you want to delete it?</p> <br />
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={deleteCategory}>Delete</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
