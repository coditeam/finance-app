import React, { useState, useEffect } from "react";
import "./Dashboard.css";
import DatePicker from "react-datepicker";

// Import Material UI
import AddIcon from "@mui/icons-material/Add";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import AccountBalanceWalletIcon from "@mui/icons-material/AccountBalanceWallet";
import PaymentsIcon from "@mui/icons-material/Payments";
import PointOfSaleIcon from "@mui/icons-material/PointOfSale";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";

import "react-datepicker/dist/react-datepicker.css";

import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

import {
  Chart as ChartJS,
  CategoryScale,
  ArcElement,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";

import { Bar } from "react-chartjs-2";
import { Pie } from "react-chartjs-2";
import ProgressBar from "@ramonak/react-progress-bar";
// import { addDays } from "date-fns";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  ArcElement,
  Title,
  Tooltip,
  Legend
);

const Dashboard = () => {
  const token = localStorage.getItem("token");
  const id = localStorage.getItem("id");
  const [startYear, setStartYear] = useState(new Date());
  const [year, setYear] = useState(2022);
  const [error, setError] = useState("");
  const [type, setType] = useState("Yearly");

  const [dateRange, setDateRange] = useState([]);
  const [startDate, endDate] = dateRange;

  const [D1, setD1] = useState(null);
  const [D2, setD2] = useState(null);

  const [goal, setGoal] = useState(null);
  const [monthlyExpenses, setMonthlyExpenses] = useState([]);
  const [monthlyIncomes, setMonthlyIncomes] = useState([]);
  const [yearlyInc, setYearlyInc] = useState([]);
  const [yearlyExp, setYearlyExp] = useState([]);
  const [catExp, setCatExp] = useState([]);
  const [catInc, setCatInc] = useState([]);
  const [comparisonData, setComparisonData] = useState([]);

  const [betweenInc, setBetweenInc] = useState([]);
  const [betweenExp, setBetweenExp] = useState([]);

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setError("");
  };

  const [goalAdded, setGoalAdded] = useState("");
  const [goalYear, setGoalYear] = useState("");

  const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const labelYearly = yearlyInc?.years;
  const labelsYearly = labelYearly?.map((num) => {
    return String(num);
  });

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Monthly Bar Chart in $",
      },
    },
  };

  const optionsYearly = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Yearly Bar Chart in $",
      },
    },
  };

  const optionsRange = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Range Chart in $",
      },
    },
  };

  const data = {
    labels,
    datasets: [
      {
        label: "Incomes",
        data: monthlyIncomes.data,
        backgroundColor: "rgba(220,220,220, 0.8)",
      },
      {
        label: "Expenses",
        data: monthlyExpenses.data,
        backgroundColor: "rgba(111, 252, 45, 0.8)",
      },
    ],
  };

  const dataYearly = {
    labels: labelsYearly,
    datasets: [
      {
        label: "Incomes",
        data: yearlyInc.yearlyIncomes,
        backgroundColor: "rgba(220,220,220, 0.8)",
      },
      {
        label: "Expenses",
        data: yearlyExp.yearlyExpenses,
        backgroundColor: "rgba(111, 252, 45, 0.8)",
      },
    ],
  };

  const labelRange = betweenExp?.dates;

  const dataRange = {
    labels: labelRange,
    datasets: [
      {
        label: "Incomes",
        data: betweenInc?.data,
        backgroundColor: "rgba(220,220,220, 0.8)",
      },
      {
        label: "Expenses",
        data: betweenExp?.data,
        backgroundColor: "rgba(111, 252, 45, 0.8)",
      },
    ],
  };
  const options1 = {
    responsive: true,
    plugins: {
      legend: {
        position: "left",
      },
      title: {
        display: true,
        text: "Categories in $",
      },
    },
  };

  const getGoal = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getGoalByUser/${id}/${year}-1-1`,
        {
          method: "GET",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);

      setGoal(fetchedData?.profitgoal[0]?.target);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getGoal();
  }, [year]);

  const addGoal = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/addGoal/${id}`,
        {
          method: "POST",
          headers: {
            authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            target: goalAdded,
            year: goalYear,
          }),
        }
      );

      const data = await response.json();
      if (data.message === "Something went really wrong!") {
        setError(data.message);
      } else {
        setError("");
      }

      if (response.status === 200) {
        console.log("goal added");
        await getGoal();
        handleClose();
        return;
      }
    } catch (error) {
      console.log(error);
    }

    setGoalAdded("");
    setGoalYear("");
  };

  const getMonthlyExpensesReports = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getMonthlyExpensesReports/${year}-1-1`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setMonthlyExpenses(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getMonthlyExpensesReports();
  }, [year]);

  const getMonthlyIncomesReports = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getMonthlyIncomesReports/${year}-1-1`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setMonthlyIncomes(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getMonthlyIncomesReports();
  }, [year]);

  var profit = (monthlyIncomes?.totalSum - monthlyExpenses?.totalSum).toFixed(
    2
  );
  if (isNaN(profit)) {
    profit = 0;
  }

  const getYearlyIncomes = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getYearlyIncomes/${year}-1-1`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setYearlyInc(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getYearlyIncomes();
  }, [year]);

  const getYearlyExpenses = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getYearlyExpenses/${year}-1-1`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setYearlyExp(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getYearlyExpenses();
  }, [year]);

  const getBetweenExpenses = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getBetweenExpenses/${D1}/${D2}`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setBetweenExp(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getBetweenExpenses();
  }, []);

  const getBetweenIncomes = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getBetweenIncomes/${D1}/${D2}`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setBetweenInc(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getBetweenIncomes();
  }, []);

  const getCatExpenses = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getCatExpenses/${year}`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      console.log(fetchedData);
      setCatExp(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getCatExpenses();
  }, [year]);

  const getCatIncomes = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getCatIncomes/${year}`,
        {
          method: "Post",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setCatInc(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getCatIncomes();
  }, [year]);

  const getComparison = async () => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/getComparison/${year}`,
        {
          method: "GET",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setComparisonData(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getComparison();
  }, [year]);

  const handleChangeRadio = (event) => {
    setType(event.target.value);
  };

  const handleYearChange = (date) => {
    setStartYear(date);
    date = new Date(
      date.getTime() - date.getTimezoneOffset() * 60000
    ).toISOString();
    date = date.slice(0, 4);
    if (date !== null && date !== undefined) {
      date = parseInt(date);
      setYear(date);

      getYearlyExpenses(date);
      getYearlyIncomes(date);

      getGoal(date);
      getCatExpenses(date);
      getCatIncomes(date);
      getComparison(date);
    }
  };
  const handleMonthChange = (date) => {
    setStartYear(date);
    date = new Date(
      date.getTime() - date.getTimezoneOffset() * 60000
    ).toISOString();
    date = date.slice(0, 4);
    if (date !== null && date !== undefined) {
      date = parseInt(date);
      setYear(date);

      getMonthlyExpensesReports(date);
      getMonthlyIncomesReports(date);

      getGoal(date);
      getCatExpenses(date);
      getCatIncomes(date);
      getComparison(date);
    }
  };

  const handleRangeChange = (update) => {
    setDateRange(update); //array of dates

    if (update[0] != null && update[1] != null) {
      setD1(
        new Date(update[0].getTime() - update[0].getTimezoneOffset() * 60000)
          .toISOString()
          .slice(0, 10)
      );
      setD2(
        new Date(update[1].getTime() - update[1].getTimezoneOffset() * 60000)
          .toISOString()
          .slice(0, 10)
      );
    }
  };

  const handleCalendarClose = () => {
    const date = D1.slice(0, 4);
    setYear(date);
    getBetweenExpenses();
    getBetweenIncomes();

    getGoal(date);
    getCatExpenses(date);
    getCatIncomes(date);
    getComparison(date);
  };
  var catLabel =
    catExp?.Category === undefined || catInc?.Category === undefined
      ? [""]
      : catExp?.Category.concat(catInc?.Category);
  var catData =
    catExp?.mergeddata === undefined || catInc?.mergeddata === undefined
      ? "[0,0,0]"
      : catExp?.mergeddata.concat(catInc?.mergeddata);
  var currentTarget =
    (profit === undefined && goal === undefined) || profit <= 0
      ? "0%"
      : Math.abs(((profit * 100) / goal).toFixed(0));

  if (!isFinite(currentTarget)) {
    currentTarget = 0;
  }

  var goals = goal;

  if (!isFinite(goals) || goal === undefined) {
    goals = 0;
  }

  var thisMonth = (comparisonData?.currentProfit * 100) / goals;
  if (thisMonth < 0) {
    thisMonth = 0;
  } else {
    thisMonth = Math.abs(thisMonth.toFixed(0));
  }

  if (!isFinite(thisMonth) || thisMonth === undefined) {
    thisMonth = 0;
  }
  var nextMonth = (comparisonData?.nextProfit * 100) / goals;
  if (nextMonth < 0) {
    nextMonth = 0;
  } else {
    nextMonth = Math.abs(nextMonth.toFixed(0));
  }
  if (!isFinite(nextMonth) || nextMonth === undefined) {
    nextMonth = 0;
  }

  catLabel = [...new Set(catLabel)];

  const data1 = {
    labels: catLabel,
    datasets: [
      {
        label: "Categories",
        data: catData,
        backgroundColor: [
          "rgba( 	49, 54 ,76, 0.8)",
          "rgba( 	110	,251	,45, 0.8)",
          "rgba( 	243	,245	,247, 0.8)",
          "rgba(	49, 54 ,76, 0.5)",
          "rgba(110	,251	,45, 0.5)",
          "rgba(243	,245	,247, 0.5)",
        ],
        borderColor: [
          "rgba( 49, 54	,76, 0.5)",
          "rgba(110	,251	,45, 0.5)",
          "rgba(243	,245	,247, 0.5)",
          "rgba( 49, 54	,76, 0.8)",
          "rgba(110	,251	,45, 0.8)",
          "rgba(243	,245	,247, 0.8)",
        ],
        borderWidth: 1,
      },
    ],
  };

  return (
    <>
      <div className="dash_container">
        <div className="dash_header">
          <div className="dash_goal">
            <h1> Profit Goal </h1>
          </div>
          <div className="dash_add">
            {" "}
            <AddIcon onClick={handleClickOpen} />
            <Dialog
              fullWidth={true}
              sx={{ overflowY: "scroll" }}
              open={open}
              onClose={handleClose}
            >
              <div className="goal_Error">
                <DialogTitle> Add Yearly Goal </DialogTitle>
                {error && (
                  <div className="error" style={{ color: "red" }}>
                    {error}
                  </div>
                )}
              </div>
              <DialogContent>
                <div className="container-category">
                  <br />

                  <label>Goal</label>
                  <br />

                  <input
                    type="text"
                    value={goalAdded}
                    placeholder="Enter your yearly goal here"
                    onChange={(e) => setGoalAdded(e.target.value)}
                    onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                  />
                  <br />

                  <br />
                  <label>Year</label>
                  <br />
                  <input
                    type="text"
                    placeholder="Enter Year between 2000 and 2050"
                    value={goalYear}
                    maxLength={4}
                    pattern="/^(20[0-0]\d|20[0-4]\d|2050)$/"
                    onChange={(e) => setGoalYear(e.target.value)}
                    onKeyPress={(event) => {
                      if (!/[0-9]/.test(event.key)) {
                        event.preventDefault();
                      }
                    }}
                  />
                  <br />
                </div>
              </DialogContent>

              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={addGoal}>Add</Button>
              </DialogActions>
            </Dialog>
          </div>
          <div className="dash_goal2">
            <AttachMoneyIcon style={{ fontSize: 40, color: "#31364c" }} />
            {goals}
          </div>
          <ProgressBar
            completed={currentTarget}
            width="100%"
            bgColor="#31364c"
            className="wrapper"
            barContainerClassName="container"
            maxCompleted={100}
            labelColor="#6efb2d"
            labelAlignment="left"
          />
        </div>

        <div className="dash_sec1">
          <div className="dash_total2">
            <div className="dash_box">
              <div className="dash_first">
                <AccountBalanceWalletIcon
                  style={{ fontSize: 40, color: "#6efb2d" }}
                />
              </div>
              <h3>Total Profit</h3>
              <div className="dash_total3">
                <AttachMoneyIcon style={{ fontSize: 40, color: "white" }} />

                {profit}
              </div>
            </div>

            <div className="dash_box">
              <div className="dash_second">
                <PaymentsIcon style={{ fontSize: 40, color: "#2b2b2b" }} />
              </div>
              <h3>Total Expenses</h3>
              <div className="dash_total3">
                <AttachMoneyIcon style={{ fontSize: 40, color: "#2b2b2b" }} />
                {monthlyExpenses?.totalSum}
              </div>
            </div>

            <div className="dash_box">
              <div className="dash_third">
                <PointOfSaleIcon style={{ fontSize: 40, color: "#2b2b2b" }} />
              </div>
              <h3>Total Incomes</h3>
              <div className="dash_total3">
                <AttachMoneyIcon style={{ fontSize: 40, color: "#2b2b2b" }} />
                {monthlyIncomes?.totalSum}
              </div>
            </div>
            <div className="dash_type">
              <h4>
                {type} Charts for {year}
              </h4>
              <FormControl className="dash__options__charts">
                <FormLabel
                  color="success"
                  id="demo-row-radio-buttons-group-label"
                ></FormLabel>
                <RadioGroup
                  className="dash__radio__charts"
                  color="success"
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                  value={type}
                  onChange={handleChangeRadio}
                >
                  <FormControlLabel
                    value="Yearly"
                    control={<Radio color="success" />}
                    label="Yearly"
                    style={{ color: "#2b2b2b" }}
                  />
                  <FormControlLabel
                    value="Monthly"
                    control={<Radio color="success" />}
                    label="Monthly"
                    style={{ color: "#2b2b2b" }}
                  />
                  <FormControlLabel
                    value="Range"
                    control={<Radio color="success" />}
                    label="Range"
                    style={{ color: "#2b2b2b" }}
                  />
                </RadioGroup>
              </FormControl>
            </div>
          </div>

          {type === "Monthly" && (
            <>
              <div className="year_picker">
                <h4>Select Year</h4>
                <DatePicker
                  selected={startYear}
                  onChange={handleMonthChange}
                  showYearPicker
                  dateFormat="yyyy"
                  yearItemNumber={5}
                />
              </div>
              <div className="dash_bar">
                <Bar options={options} data={data} />
              </div>
            </>
          )}
          {type === "Yearly" && (
            <>
              <div className="year_picker">
                <h4>Select Year</h4>
                <DatePicker
                  selected={startYear}
                  onChange={handleYearChange}
                  showYearPicker
                  dateFormat="yyyy"
                  yearItemNumber={5}
                />
              </div>
              <div className="dash_bar">
                <Bar options={optionsYearly} data={dataYearly} />
              </div>
            </>
          )}
          {type === "Range" && (
            <>
              <div className="dash_ranger">
                <h4>Select Start and End Date</h4>
                <DatePicker
                  selectsRange={true}
                  dateFormat="yyyy/MM/dd"
                  startDate={startDate}
                  endDate={endDate}
                  onChange={handleRangeChange}
                  withPortal
                  onCalendarClose={handleCalendarClose}
                />
              </div>

              <div className="dash_bar">
                <Bar options={optionsRange} data={dataRange} />
              </div>
            </>
          )}

          <div className="dash_sec2">
            <Pie options={options1} data={data1} />
            <div className="dash_comparison">
              <h3>Comparison(Month)</h3>
              <div className="dash_thismonth">
                This month
                <ProgressBar
                  completed={thisMonth}
                  width="100%"
                  bgColor="#31364c"
                  className="wrapper"
                  barContainerClassName="container"
                  labelColor="#6efb2d"
                  labelAlignment="left"
                />
              </div>
              <div className="dash_lastmonth">
                Last month
                <ProgressBar
                  completed={nextMonth}
                  width="100%"
                  bgColor="#31364c"
                  className="wrapper"
                  barContainerClassName="container"
                  maxCompleted={100}
                  labelColor="#6efb2d"
                  labelAlignment="left"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;
