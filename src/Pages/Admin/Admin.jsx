import React from "react";
import { useState, useEffect } from "react";
import "../../components/admin/AdminsCard.css";
import AdminsCard from "../../components/admin/AdminsCard";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import AddIcon from "@mui/icons-material/Add";

const Admin = () => {
  const [adminsData, setAdminsData] = useState([]);
  const [adminsName, setAdminsName] = useState([]);

  const [error, setError] = useState("");
  const token = localStorage.getItem("token");
  const [query, setQuery] = useState("");
  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);
  const [childData, setChildData] = React.useState(null); // the lifted state

  const [name, setName] = useState(childData?.admin ? childData?.admin : "");
  const [email, setEmail] = useState(childData?.email ? childData?.email : "");
  const [password, setPassword] = useState(
    childData?.password ? childData?.password : ""
  );

  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const sendDataToParent = (index) => {
    // the callback. Use a better name

    setChildData(index);
  };
  const handleClickOpen1 = () => {
    setOpen1(true);
    setNameError("");
    setEmailError("");
    setPasswordError("");
  };

  const handleClose1 = async () => {
    setOpen1(false);

    setError("");
    setName("");
    setEmail("");
    setPassword("");
  };

  const handleClickOpen = () => {
    setOpen(true);
    setName(childData?.admin);
    setEmail(childData?.email);
    setPassword(childData?.password);
    setNameError("");
    setEmailError("");
    setPasswordError("");
  };

  const handleClose = async () => {
    setOpen(false);

    setNameError("");
    setEmailError("");
    setPasswordError("");
    setError("");
    // setName("");
    // setEmail("");
    // setPassword("");
  };

  const updateAdmin = async (props) => {
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/updateAdmin/${childData.id}`,
        {
          method: "PUT",
          headers: {
            authorization: `Bearer ${token}`,
            "Access-Control-Allow-Origin": "*",
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: name,
            email: email,
            password: password,
          }),
          _method: "PUT",
        }
      );
      const data = await response.json();
      console.log(data.message);
      if (response.status === 200) {
        console.log("updated");
        await getAdmins();
        setError("");
        setNameError("");
        setEmailError("");
        setPasswordError("");
        setOpen(false);
        return;
      }
      if (response.status === 400) {
        setError(data.message);
        return;
      }
      if (response.status === 401) {
        alert("Not Authorized to edit!");
        setError(data.errors);
        return;
      } else {
        setError(data.message);
        setNameError(data?.errors?.name);
        setEmailError(data?.errors?.email[0]);
        setPasswordError(data?.errors?.password[0]);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const getAdmins = async () => {
    try {
      const response = await fetch(
        "https://boiling-escarpment-75862.herokuapp.com/api/getAllAdmins",
        {
          method: "GET",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setAdminsData(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getAdmins();
  }, []);

  const getAdminsByName = async () => {
    try {
      const response = await fetch(
        "https://boiling-escarpment-75862.herokuapp.com/api/getAdminName/{name}",
        {
          method: "GET",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      const fetchedData = await response.json(response);
      setAdminsName(fetchedData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getAdminsByName();
  }, []);

  const deleteAdmin = async (id) => {
    console.log("delete user");
    try {
      const response = await fetch(
        `https://boiling-escarpment-75862.herokuapp.com/api/deleteAdmin/${id}`,
        {
          method: "DELETE",
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.status === 200) {
        console.log("deleted");
        await getAdmins();
        return;
      }
    } catch (e) {
      console.log(e.message);
    }
  };

  const addAdmin = async () => {
    try {
      const response = await fetch(
        "https://boiling-escarpment-75862.herokuapp.com/api/register",
        {
          method: "POST",
          headers: {
            authorization: `Bearer ${localStorage.getItem("token")}`,
            "Access-Control-Allow-Origin": "*",
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: name,
            email: email,
            password: password,
          }),
        }
      );

      const data = await response.json();

      if (response.status === 200) {
        console.log("added");
        await getAdmins();
        setError("");
        setNameError("");
        setPasswordError("");
        setOpen1(false);
        return;
      } else if (response.status === 500) {
        setEmailError("Email already used !");
      } else {
        setError("Invalid Inputs");
        setNameError(data?.errors?.name);
        if (data?.errors?.email !== undefined) {
          setEmailError(data.errors.email[0]);
        } else {
          setEmailError("");
        }

        if (data?.errors?.password !== undefined) {
          setPasswordError(data.errors.password[0]);
        } else {
          setPasswordError("");
        }
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className="searchBar_Header">
        <input
          className="search_Bar"
          placeholder="Search for User"
          onChange={(event) => setQuery(event.target.value)}
        />

        <div className="add_admin">
          <AddIcon
            size={30}
            onClick={() => {
              handleClickOpen1();
            }}
          />
        </div>
      </div>
      {adminsData.users &&
        (adminsData.users || [])
          .filter((admins) => {
            if (query === "") {
              return admins;
            } else if (admins.name.includes(query)) {
              return admins;
            }
          })
          .reverse()
          .map((admins) => (
            <AdminsCard
              id={admins.id}
              admin={admins.name}
              deleteAdmin={deleteAdmin}
              addAdmin={addAdmin}
              updateAdmin={updateAdmin}
              handleClickOpen={handleClickOpen}
              sendDataToParent={sendDataToParent}
              email={admins.email}
              password={admins.password}
            />
          ))}

      <Dialog
        fullWidth={true}
        maxWidth={"md"}
        open={open}
        onClose={handleClose}
        sx={{ overflowY: "scroll" }}
      >
        <DialogTitle>Edit Admin Data</DialogTitle>
        {/* className="error" */}

        <DialogContent>
          {error && <div style={{ color: "red" }}>{error}</div>}
          {nameError && (
            <div className="name_Error" style={{ color: "red" }}>
              {nameError}
            </div>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
            variant="standard"
            defaultValue={childData?.admin}
            onChange={(e) => {
              setName(e.target.value);
            }}
            error={nameError}
            // helperText={error}
          />
        </DialogContent>

        <DialogContent>
          {emailError && (
            <div className="error" style={{ color: "red" }}>
              {emailError}
            </div>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email"
            type="email"
            fullWidth
            variant="standard"
            defaultValue={childData?.email}
            onChange={(e) => setEmail(e.target.value)}
            error={emailError}
            // helperText={error}
          />
        </DialogContent>
        <DialogContent>
          {passwordError && (
            <div className="error" style={{ color: "red" }}>
              {passwordError}
            </div>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Password"
            type="password"
            fullWidth
            variant="standard"
            defaultValue={childData?.password}
            onChange={(e) => setPassword(e.target.value)}
            error={passwordError}
            // helperText={error}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={updateAdmin}>Edit</Button>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
      </Dialog>

      {/*  */}

      <Dialog
        fullWidth={true}
        maxWidth={"md"}
        open={open1}
        onClose={handleClose1}
        sx={{ overflowY: "scroll" }}
      >
        <DialogTitle>Add Admin</DialogTitle>

        <DialogContent>
          {error && <div style={{ color: "red" }}>{error}</div>}
          {nameError && (
            <div className="name_Error" style={{ color: "red" }}>
              {nameError}
            </div>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="text"
            fullWidth
            variant="standard"
            onChange={(e) => {
              setName(e.target.value);
            }}
            error={nameError}
          />
        </DialogContent>

        <DialogContent>
          {emailError && (
            <div className="error" style={{ color: "red" }}>
              {emailError}
            </div>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email"
            type="email"
            fullWidth
            variant="standard"
            onChange={(e) => setEmail(e.target.value)}
            error={emailError}
          />
        </DialogContent>
        <DialogContent>
          {passwordError && (
            <div className="error" style={{ color: "red" }}>
              {passwordError}
            </div>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="password"
            label="Password"
            type="password"
            fullWidth
            required
            variant="standard"
            onChange={(e) => setPassword(e.target.value)}
            error={passwordError}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={addAdmin}>Add</Button>
          <Button onClick={handleClose1}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Admin;
