import React from "react";
import "./AdminsCard.css";

import { MdOutlineDeleteOutline } from "react-icons/md";
import ModeEditIcon from "@mui/icons-material/ModeEdit";

const AdminsCard = (props) => {

  return (
    <>
      <table className="table table-action">
        <thead>
          {/* <tr>
            <th className="t-icon"></th>
            <th className="t-icon"></th>
            <th className="t-small">Admin Name</th>
          </tr> */}
        </thead>
        <tbody  className="table_body">
          {/* {props.map((data, index) => {
            return ( */}
          <tr>
            <td>
              <MdOutlineDeleteOutline
                size={30}
                color={"red"}
                onClick={() => {
                  props.deleteAdmin(props.id);
                }}
              />
            </td>
            <td>
              <ModeEditIcon
                size={30}
                onClick={() => {
                  props.handleClickOpen(props.id);
                  props.sendDataToParent(props);
                }}
              />
            </td>
            <td>{props.admin}</td>
          </tr>
          {/* );
          })} */}
        </tbody>
      </table>
    </>
  );
};


export default AdminsCard;
