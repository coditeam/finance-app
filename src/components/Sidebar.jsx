import React, { useState } from "react";
import { FaBars } from "react-icons/fa";
import { MdDashboard, MdCategory } from "react-icons/md";
import { RiAdminLine } from "react-icons/ri";
import { NavLink } from "react-router-dom";
import { CgLogOut } from "react-icons/cg";
import { BsArrowUp, BsArrowDown } from "react-icons/bs";
import image from "../components/Logo/finance-logo.png";
import { ContentPasteSearchOutlined } from "@mui/icons-material";

const Sidebar = ({ children }) => {
  const [isOpen, setIsOpen] = useState(true);
  const toggle = () => setIsOpen(!isOpen);
  const superId = localStorage.getItem("id");
  const [isLoggedin, setIsLoggedin] = useState(false);

  const isSuper = () => {
    if ( superId === '1')
    return true; 
  }



    // if(props?.target?.firstChild?.data === 'Logout')
    

  
  const menuItem = [
    {
      path: "/dashboard/superadmin",
      name: "Dashboard",
      icon: <MdDashboard />,
    },
    {
      path: "/category",
      name: "Category",
      icon: <MdCategory />,
    },
    {
      path: "/expenses",
      name: "Expenses",
      icon: <BsArrowDown />,
    },
    {
      path: "/incomes",
      name: "Incomes",
      icon: <BsArrowUp />,
    },
    ...(isSuper()
      ? [
          {
            path: "/superadmin",
            name: "Admin",
            icon: <RiAdminLine />,
          },
        ]
      : []),

    // {
    //   path: "/",
    //   name: "Logout",
    //   icon: <CgLogOut />,
    // },
  ];

  return (
    <div className="container">
      <div style={{ width: isOpen ? "280px" : "50px" }} className="sidebar">
        <div className="top_section">
          <div style={{ marginLeft: isOpen ? "50px" : "0px" }} className="bars">
            <FaBars onClick={toggle} />
          </div>
          <h1 style={{ display: isOpen ? "block" : "none" }} className="logo">
            <img src={image}  height={120} width={160} />
          </h1>
        </div>
        <div className="side_container">
        <div className="side_map">
        {menuItem.map((item, index) => (
          <NavLink
            to={item.path}
            key={index}
            className="link"
            activeclassName="active"
            
          >
            <div className="icon">{item.icon}</div>
            <div
              style={{ display: isOpen ? "block" : "none" }}
              className="link_text"
            >
              {item.name}
            </div>
          </NavLink>
        ))}
        </div>
        <div className="side_logout">
        <NavLink
          to="/"
          // key=
          className='link'
          activeclassName='active'
          >
          <div className="icon">{<CgLogOut />}</div>

          <div
              style={{ display: isOpen ? "block" : "none" }}
              className="link_text"
            >
              Logout
            </div>
          </NavLink>
          </div>
          </div>
      </div>
      <main>{children}</main>
    </div>
  );
};

export default Sidebar;
