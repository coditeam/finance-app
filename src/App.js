import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import LoginPage from "./Pages/LoginPage";
import Sidebar from "./components/Sidebar";
import Dashboard from "./Pages/Dashboard/Dashboard";
import Admin from "./Pages/Admin/Admin";
import Incomes from "./Pages/Incomes/Incomes";
import Expenses from "./Pages/Expenses/Expenses";
import Category from "./Pages/Category/Category";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route
            path="/dashboard/superadmin"
            element={
              <Sidebar>
                <Dashboard />
              </Sidebar>
            }
          />
          <Route
            path="/category"
            element={
              <Sidebar>
                <Category />
              </Sidebar>
            }
          />
          <Route
            path="/expenses"
            element={
              <Sidebar>
                <Expenses />
              </Sidebar>
            }
          />
          <Route
            path="/incomes"
            element={
              <Sidebar>
                <Incomes />
              </Sidebar>
            }
          />
          <Route
            path="/superadmin"
            element={
              <Sidebar>
                <Admin />
              </Sidebar>
            }
          />
        </Routes>
      </Router>
    </>
  );
}
export default App;
